/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vaiouser
 */
/**@RunWith(value = Parameterized.class)*/
public class TestClassReadTest {
    private String a;
    private String b;
    private String op;
    private String res;
    
    public TestClassReadTest(String a, String b, String op, String res) {
        this.a = a;
        this.b = b;
        this.op = op;
        this.res = res;
    }
    
    /**@Parameterized.Parameters
    public static ArrayList<String[]> testData() {

        final ArrayList<String[]> data = TestClassRead.readFile("C:/\Users//\Vaiouser/\Desktop/\JAVA/\test/\test.txt");

        return data;
    }
    /**
     * Test of readFile method, of class TestClassRead.
     */
    @Test
    public void testReadFile() {
        System.out.println("readFile");
        switch (op) {
            case "+":
                {
                    System.out.print("Testintg operation ADD");
                    int expResult = Integer.parseInt(res);
                    int actResult = Integer.parseInt(a) + Integer.parseInt(b);
                    assertEquals(expResult, actResult);
                    break;
                }
            case "-":
                {
                    System.out.print("Testing operation substract");
                    int expResult = Integer.parseInt(res);
                    int actResult = Integer.parseInt(a) - Integer.parseInt(b);
                    assertEquals(expResult, actResult);
                    break;
                }
            case "*":
                {
                    System.out.print("Testing operation multiple");
                    int expResult = Integer.parseInt(res);
                    int actResult = Integer.parseInt(a) * Integer.parseInt(b);
                    assertEquals(expResult, actResult);
                    break;
                }
            case "/":
                {
                    System.out.print("Tasting operation division");
                    int expResult = Integer.parseInt(res);
                    int actResult = Integer.parseInt(a) / Integer.parseInt(b);
                    assertEquals(expResult, actResult);
                    break;
                }
            default:
                System.out.println("Error read operation");
                break;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
